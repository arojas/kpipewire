# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kpipewire package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kpipewire\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-25 01:41+0000\n"
"PO-Revision-Date: 2022-07-13 15:41+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: pipewirecore.cpp:98
#, kde-format
msgid "Failed to create PipeWire context"
msgstr "PipeWire 콘텍스트를 만들 수 없음"

#: pipewirecore.cpp:115
#, kde-format
msgid "Failed to connect to PipeWire"
msgstr "PipeWire 콘텍스트에 연결할 수 없음"

#: pipewirecore.cpp:122
#, kde-format
msgid "Failed to start main PipeWire loop"
msgstr "주 PipeWire 루프를 시작할 수 없음"
